package com.comp.master.view;

import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SiteController {

	final Logger LOG = LoggerFactory.getLogger(SiteController.class);
	
	@RequestMapping(value = "/")
	public ModelAndView mainPage() {
		LOG.info("inside mainPage");
		return new ModelAndView("home");
	}

	@RequestMapping(value = "/contactus")
	public ModelAndView contactusPage(ModelMap map, HttpServletRequest request) {
		LOG.info("inside contactusPage");
		return new ModelAndView("contactus");
	}

	@RequestMapping(value = "/aboutus")
	public ModelAndView aboutusPage(ModelMap map, HttpServletRequest request) {
		LOG.info("inside aboutusPage");
		return new ModelAndView("aboutus");
	}
}
